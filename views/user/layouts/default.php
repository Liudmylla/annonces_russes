<!DOCTYPE html>

<html lang="fr" class="h-100">



<head>

    <meta charset="UTF-8">

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?= isset($title) ? e($title) : 'Mon site' ?></title>

    <link href="https://fonts.googleapis.com/css?family=Merriweather:700i&display=swap" rel="stylesheet">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">





</head>



<body class="d-flex flex-column h-100">

    <div class="container mt-2 mb-2">

        <div class=" d-flex flex-row bd-highlight justify-content-center mb-4" style=" background: no-repeat top url('<?= ASSETS; ?>images/base@2x.jpg')">

            <div class="p-2 bd-highlight ">

                <a href="<?= $router->url('user_posts', ['id_user' =>  $_SESSION['id_user']]) ?>" class="navbar-brand">

                    <img src="<?= ASSETS; ?>images/kokoshnik40.png" alt="Espace membre" />

                </a>

            </div>

            <div class="p-2 bd-highlight ">

                <h1 style="text-transform:capitalize;color:#DA4453; font-family:'Merriweather',serif;font-size:36px;">Bienvenue <?php echo $_SESSION['auth']; ?>!</h1>

            </div>

            <div class="p-2 bd-highlight ">

                <form action="<?= $router->url('logout') ?>" method="post" onsubmit="return confirm('Voulez vous vraiement se déconnecter ?')" style=" display:inline">

                    <button type="submit" style="background:transparent; border:none;"><img src=" <?= ASSETS; ?>images/kremlin40.png" alt="Se deconnecter"></button>

                </form>

            </div>

        </div>

    </div>
<div class="container mt-4">
        <?= $content ?>

    </div>

    <footer class="bg-light py-4 footer mt-auto">

        <!-- <div class="container">

            <?php if (defined('DEBUG_TIME')) : ?>

                Page générée en <?= round(1000 * (microtime(true) - DEBUG_TIME)) ?> ms.

            <?php endif ?>

        </div> -->

    </footer>

</body>

</html>