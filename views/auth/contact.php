<?php
//проверяем, существуют ли переменные в массиве POST

if (isset($_POST['send'])) {

    if (isset($_POST['name']) and isset($_POST['email'])) {
        //показываем форму
        $name = $_POST['name'];
        $email = $_POST['email'];
        $content = $_POST['content'];
        $name = htmlspecialchars($name);
        $email = htmlspecialchars($email);
        $content = htmlspecialchars($content);
        $name = urldecode($name);
        $email = urldecode($email);
        $content = urldecode($content);
        $name = trim($name);
        $email = trim($email);
        $content = trim($content);
    }
    // Если все поля заполнены, то можем отправлять письмо .
    if ($name != " " && $email != " " && $content != " ") {

        $header = "Content-type:text/plain; charset = UTF-8\r\nFrom:$name <$email>"; // Указываем имя и email отправителя.

        // Отправляем письмо
        $send = mail('info@russe64.fr', '$name', $content, $header);
        $result = null;
        if ($send) {
            $result = 'Votre message a bien été envoyé';
        } else {
            $result = 'Une erreur est survenue!';
        }
        header('Refresh: 5; URL=http://www.russe64.fr');
    }
}
?>
<div style="text-align: center">
    <h1 style="color:#DA4453; font-family:'Merriweather',serif;font-size:22px;">Formulaire de contact</h1>
    <?php if ($result !== null) : ?>
        <div><b><?= $result ?></b></div>
        <div style="color:#DA4453; font-family:'Merriweather',serif;font-size:12px;margin-top:8px;">Vous serez redirigé vers la page d'accueil dans 5 secondes.</div>
    <?php endif; ?>
    <form method="post" action="" class="postcard">
        <div class="form-row">
            <label for="name">Votre nom</label><input type="text" id="name" name="name" required>
        </div>
        <div class="form-row">
            <label for="email">Votre email</label><input type="text" id="email" name="email" required>
        </div>
        <div class="form-row">
            <label for="content">Votre message</label><textarea rows="5" id="content" name="content" required></textarea>
        </div>
        <div class="form-row">
            <input type="submit" name="send" value="Envoyer">
        </div>
    </form>
</div>