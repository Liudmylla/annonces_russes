<?php

use App\Connection;

$pdo = Connection::getPDO();

$id_user = $_GET['id'];
$token = $_GET['token'];

$req = $pdo->prepare('SELECT * FROM user WHERE id_user = ?');
$req->execute([$id_user]);
$user = $req->fetch();
session_start();

if($user && $user->confirmation_token == $token ){
$pdo->prepare('UPDATE user SET confirmation_token = NULL, confirmed_at = NOW() WHERE id_user = ?')->execute([$id_user]);
$_SESSION['flash']['success'] = 'Votre compte a bien été validé';
$_SESSION['auth'] = $user;
    header('Location: ' . $router->url('user_posts', ['id_user' => $_SESSION['id_user']]));
}else{
$_SESSION['flash']['danger'] = "Ce token n'est plus valide";
    header('Location: ' . $router->url('login'));
}