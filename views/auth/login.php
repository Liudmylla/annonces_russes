 <?php

    use App\Connection;
    use App\Model\User;
    use App\HTML\Form;
    use App\Table\Exception\NotFoundException;
    use App\Table\UserTable;

    $user = new User();

    $errors = [];
    if (!empty($_POST)) {
        $user->setUsername($_POST['username']);
        $errors['password'] = 'Identifiant ou mot de passe incorrect';
        if (!empty($_POST['username']) && !empty($_POST['password'])) {
            $table = new UserTable(Connection::getPDO());

            try {
                $u = $table->findByUsername($_POST['username']);
                if (password_verify($_POST['password'], $u->getPassword()) === true) {
                    session_start();
                    $_SESSION['auth'] = $u->getUsername();
                    $_SESSION['id_user'] = $u->getIdUser();
                    if ($_SESSION['auth'] !== 'admin') {
                        header('Location: ' . $router->url('user_posts', ['id_user' => $_SESSION['id_user']]));
                        die();
                    }
                    if ($_SESSION['auth'] === 'admin') {
                        header('Location: ' . $router->url('admin_posts'));
                        die();
                    }
                };
            } catch (NotFoundException $e) {
            }
        }
    }
    $form = new Form($user, $errors);
    ?>
 <div style="text-align: center">
     <h1 style="color:#DA4453; font-family:'Merriweather',serif;font-size:22px;">Se connecter</h1>
     <?php if (isset($_GET['forbidden'])) : ?>
         <div class="alert alert-danger">
             Vous ne pouvez pas accéder à cette page
         </div>
     <?php endif ?>
     <form action="<?= $router->url('login') ?>" method="POST">
         <?= $form->input('username', 'Pseudo ou votre adresse email'); ?>
         <?= $form->input('password', 'Mot de passe'); ?>
         <button type="submit" class="btn btn-primary mt-4">Se connecter</button>
         <div style="margin:30px 0 450px;" class="box-register">Vous étes nouveau ici?
             <a href="<?= $router->url('register') ?>">S'inscrire</a>
         </div>
     </form>
</div>