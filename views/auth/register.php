<?php

use App\Connection;
use App\Table\UserTable;

session_start();
$pdo = Connection::getPDO();

if (!empty($_POST)) {
    $errors = array();

    if (empty($_POST['username']) || !preg_match('/^[a-zA-Z0-9_]+$/', $_POST['username'])) {
        $errors['username'] = "Votre pseudo n'est pas valide";
    } else {
        $sql = $pdo->prepare("SELECT id_user from user WHERE username = ?");
        $sql->execute([$_POST['username']]);
        $user = $sql->fetch();
        if ($user) {
            $errors['username'] = 'Ce pseudo est deja pris';
        }
    }

    if (empty($_POST['email']) || !filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)) {
        $errors['email'] = "Votre email n'est pas valide";
    } else {
        $sql = $pdo->prepare("SELECT id_user from user WHERE email = ?");
        $sql->execute([$_POST['email']]);
        $user = $sql->fetch();
        if ($user) {
            $errors['email'] = 'Cet email est deja utilisé pour un autre compte';
        }
    }
    // regex validation password:1 minuscule 1 majuscule 1 chiffre 1 caractère spécial mini 6 caractères /((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{8,50})/
    if (
        empty($_POST['password'])
        || preg_match('/((?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\W]).{6,50})/', $_POST['password'])
        || $_POST['password'] != $_POST['password_confirm']
    ) {
        $errors['password'] = "Votre password n'est pas valide";
    }
    if (empty($errors)) {

        //$req = $pdo->prepare("INSERT INTO user SET username = ?, password=?,email=?");
        $req = $pdo->prepare("INSERT INTO user SET username = ?, password=?,email=?,confirmation_token=?");
        $password = password_hash($_POST['password'], PASSWORD_BCRYPT);
        $token = UserTable::str_random(60);
        $req->execute([$_POST['username'], $password, $_POST['email'], $token]);
        //$req->execute([$_POST['username'], $password, $_POST['email']]);
        $id_user = $pdo->lastInsertId();
        $send = mail($_POST['email'], 'Confirmation de votre compte', "Afin de valider votre compte merci de cliquer sur ce lien:  https://russe64.fr/confirme.php?id=$id_user&token=$token");
        $result = null;

        if ($send) {

            $result = 'Vous pouvez vous connecter sur votre compte';
        } else {

            $result = 'Une erreur est survenue!';
        }

        header('Refresh: 5; URL=http://www.russe64.fr/login');
    }
}
?>
<div style="text-align: center">
    <h1 style="color:#DA4453; font-family:'Merriweather',serif;font-size:22px;">S'inscrire</h1>
    <?php if ($result !== null) : ?>
        <div style="color:#007BFF; font-family:'Merriweather',serif;font-size:18px;"><b><?= $result ?></b></div>
        <div style="color:#DA4453; font-family:'Merriweather',serif;font-size:12px;margin-top:8px;">Vous serez redirigé vers la page de login dans 5 secondes.</div>
    <?php endif; ?>
    <?php if (!empty($errors)) : ?>
        <div class="alert alert-danger">
            <p>Votre compte n'a pas pu etre enregistré, merci de corriger vos erreurs </p>
            <ul>
                <?php foreach ($errors as $error) : ?>
                    <li><?= $error; ?></li>
                <?php endforeach; ?>
            </ul>
        </div>
    <?php endif; ?>
    <form action="" method="POST">
        <div class="form-group">
            <label for="">Pseudo</label>
            <div>Utilisez l'alphabeth latin, chiffres et _.</div>
            <input type="text" name="username" placeholder="" class="form-control" />
        </div>
        <div class="form-group">
            <label for="">Email</label>
            <input type="text" name="email" class="form-control" />
        </div>
        <div class="form-group">
            <label for="">Mot de passe</label>
            <div>Minimum 6 caractères avec 1 majuscule, 1 chiffre et 1 caractère spécial.</div>
            <input type="password" name="password" placeholder=" " class="form-control" />
        </div>
        <div class="form-group">
            <label for="">Confirmez votre mot de passe</label>
            <input type="password" name="password_confirm" class="form-control" />
        </div>
        <button type="submit" class="btn btn-primary" onsubmit="return confirm('Merci de vérifier votre email')">M'inscrire</button>
        <div style="margin:30px 0 400px;" class=" box-register mt-4 mb-4">Déjà inscrit?
            <a href="<?= $router->url('login') ?>">Connectez-vous ici</a>
        </div>
    </form>
</div>