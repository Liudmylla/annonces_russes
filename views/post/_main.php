<div class='container'>
    <div class="row">


        <div class="col-md-4">
            <div class="card mb-3">

                <img src="<?= ASSETS; ?>images/okean.jpg" alt="">

                <h5 style="text-align:center;font-size:18px;font-family:Merriweather;" class="card-title mt-2">
                  <a href="<?= $router->url('contact') ?>" title="Contact"> Les amis de la culture russe à Biarritz</a> </h5>
                <div style="text-align:center;color:#DA4453;font-family:Merriweather;font-size:16px;font-weight:300;" class="card-body ">
                    L'association interculturelle "Les amis de la culture russe" à Biarritz est
                    ouverte à tous ceux qui s'intéressent à la culture et à la langue russe. 
                </div>
                <div style="color:#007BFF;font-family:Merriweather;font-size:12px;text-align:center;" class="card-body ">
                    Pour voir nos activités par catégorie cliquez sur le lien Catégory,  pour révenir sur la page d'accueil cliquez sur
                    <a href="<?= $router->url('home') ?>" title="Espace membre">
                        <img src="<?= ASSETS; ?>images/kremlin40.png" alt="Accueil" />
                    </a>
                 
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card mb-3">
                
                    <img src="<?= ASSETS; ?>images/moskva.jpg" alt="">
         
                <h5 style="text-align:center;font-size:18px;font-family:Merriweather;" class="card-title mt-2">
                    <a href=" https://www.instagram.com/les_chales/"> Les danses folkloriques russes </a> </h5>
                <div style="text-align:center;color:#DA4453;font-family:Merriweather;font-size:16px;font-weight:300;" class="card-body ">
                    L'association "Les amis de le culture russe" à Biarritz avec son ensemble "Les_Chales" vous invit à participer à des danses russes.<a href="https://www.instagram.com/les_chales/" target="_blank">
                <img src="<?= ASSETS; ?>images/instagram.png" alt="instagram">
            </a>
                </div>
                <div style="text-align:center;color:#007BFF;font-family:Merriweather;font-size:14px;" class="card-body ">
                    Contact: <a style=" color:#DA4453;" href="<?= $router->url('contact') ?>" title="Contact">info@russe64.fr</a>
                </div>
            </div>
        </div>
        <div class=" col-md-4">
            <div class="card mb-3">
             
                    <img src="<?= ASSETS; ?>images/knigi.jpg" alt="">
       
                <h5 style="text-align:center;font-size:18px;font-family:Merriweather;" class="card-title mt-2">
                    <a href=" https://www.instagram.com/parlons_russe/"> Les cours de russe </a> </h5>
                <div style="text-align:center;color:#DA4453;font-family:Merriweather;font-size:16px;font-weight:300;" class="card-body ">
                    Vous souhaitez en savoir plus sur la langue Russe?
                    Notre association vous propose de regarder les publications et ainsi comprendre cette langue.<a href="https://www.instagram.com/parlons_russe/" target="_blank">
                <img src="<?= ASSETS; ?>images/instagram.png" alt="instagram">
            </a>
                </div>
                <div style="color:#007BFF;font-family:Merriweather;font-size:12px;text-align:center;" class="card-body ">
                    Pour créer votre espace membre ou pour se connecter cliquez sur
                    <a href="<?= $router->url('login') ?>" title="Espace membre">
                        <img src="<?= ASSETS; ?>images/kokoshnik40.png" alt="Espace membre" />
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>